package com.cloudsherpas.twitter.config;

import com.cloudsherpas.twitter.services.TweetService;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

@Configuration
@Lazy
public class ServiceAppContext {

    @Bean(name="tweetService")
    public TweetService getTweetService(){
        return new TweetService();
    }

}
