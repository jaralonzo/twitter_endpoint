package com.cloudsherpas.twitter.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import com.cloudsherpas.twitter.dao.TweetDao;
import com.cloudsherpas.twitter.dao.impl.TweetDaoImpl;

@Configuration
@Lazy
public class DaoAppContext {

    @Bean(name="tweetDao")
    public TweetDao getTweetDao(){
        return new TweetDaoImpl();
    }

}
