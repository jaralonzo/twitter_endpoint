package com.cloudsherpas.twitter.model;

import java.util.List;

/**
 * Created by jalonzo on 12/2/15.
 */
public class TweetResponse {


    Long maxId; //default = 1
    String nextPageQuery;
    List<String> oembed;

    public String getNextPageQuery() {
        return nextPageQuery;
    }

    public void setNextPageQuery(String nextPageQuery) {
        this.nextPageQuery = nextPageQuery;
    }

    public Long getMaxId() {
        return maxId;
    }

    public void setMaxId(Long maxId) {
        this.maxId = maxId;
    }

    public List<String> getOembed() {
        return oembed;
    }

    public void setOembed(List<String> oembed) {
        this.oembed = oembed;
    }


}
