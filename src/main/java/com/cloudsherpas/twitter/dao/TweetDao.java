package com.cloudsherpas.twitter.dao;

import java.util.List;

import com.cloudsherpas.twitter.model.Tweet;

public interface TweetDao extends BaseDao<Tweet>  {

}
