package com.cloudsherpas.twitter.dao;

import com.cloudsherpas.twitter.model.Tweet;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.impl.translate.opt.joda.JodaTimeTranslators;


public class DaoManager {

    private static DaoManager self;

    static {
        registerEntities();
    }

    private DaoManager() {
    }

    public static DaoManager getInstance() {
        if (self == null) {
            self = new DaoManager();
        }

        return self;
    }

    private static void registerEntities() {
        ObjectifyService.begin();
        
        ObjectifyService.factory().register(Tweet.class);
    }

    public Objectify getObjectify() {
        return ObjectifyService.ofy();
    }
}
