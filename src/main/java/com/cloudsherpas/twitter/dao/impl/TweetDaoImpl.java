package com.cloudsherpas.twitter.dao.impl;

import java.util.List;

import com.cloudsherpas.twitter.dao.TweetDao;
import com.cloudsherpas.twitter.model.Tweet;

public class TweetDaoImpl extends BaseDaoImpl<Tweet> implements TweetDao {

		public TweetDaoImpl(){
			super(Tweet.class);
		}
}
