package com.cloudsherpas.twitter.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cloudsherpas.twitter.model.TweetResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;

import twitter4j.OEmbedJSONImpl;
import twitter4j.TwitterException;

import com.cloudsherpas.twitter.dto.TweetDTO;
import com.cloudsherpas.twitter.model.Tweet;
import com.cloudsherpas.twitter.services.TweetService;
import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.Named;

@Api
(
		name ="twitterzzz",
		version = "v1",
		description = "Google End Point for Twitter4j"
)
public class TweetServiceAPI {
	
    @Autowired
    @Lazy
    @Qualifier("tweetService")
	private TweetService tweetService;
	
	@ApiMethod
	(
			name = "tweet.add",
	        path = "tweet/add",
	        httpMethod = ApiMethod.HttpMethod.PUT
	)
	public Map<String, Long> addTweet(final TweetDTO tweetDTO) {

		final Long key = tweetService.addTweet(tweetDTO);
		
        final Map<String, Long> result = new HashMap<>();
        
        if (key != null){
            result.put("key", key);
        }

        return result;
    }
	
	@ApiMethod
	(
			name = "tweet.getByHashtag",
			path = "tweet/get",
			httpMethod = ApiMethod.HttpMethod.GET
	)
	public TweetResponse getByHashtag (@Named("hashtag") final String key,
									   @Named("nextPageQuery") final String nextPageQuery,
									   @Named("maxId") final Long maxId) throws TwitterException{
		return tweetService.getByHashtag(key, nextPageQuery, maxId);
	}

}
