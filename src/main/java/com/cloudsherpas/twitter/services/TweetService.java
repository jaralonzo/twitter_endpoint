package com.cloudsherpas.twitter.services;

import java.util.ArrayList;
import java.util.List;

import com.cloudsherpas.twitter.model.TweetResponse;
import com.sun.org.apache.xpath.internal.SourceTree;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;

import twitter4j.OEmbedJSONImpl;
import twitter4j.OEmbedRequest;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.api.TweetsResources;
import twitter4j.conf.ConfigurationBuilder;

import com.cloudsherpas.twitter.dao.TweetDao;
import com.cloudsherpas.twitter.dto.TweetDTO;
import com.cloudsherpas.twitter.model.Tweet;
import com.google.appengine.api.search.query.ExpressionParser.negation_return;

public class TweetService {
	
//	public static List<Tweet> tweets = new ArrayList<Tweet>();

    @Autowired()
    @Qualifier("tweetDao")
    @Lazy
    private TweetDao tweetDao;
    
    private Twitter twitter;

    private ModelMapper modelMapper;

    public TweetService() {
        modelMapper = new ModelMapper();
        connect();
           
    }

  	private void connect() {
  		
  		String oAuthConsumerKey = "R3zjgYcnUtSMWWhMpWwvZBRYz";
		String oAuthConsumerSecret = "tA9jaAyFZoYNii7yd3X6mpwdRMsixm44quZ22slkjBRcJIUixW";
		String oAuthAccessToken = "38982504-ei8Ry2B9rdhsBe8wkKyD9HTyUvb15gQRA2qtHw6Xr";
		String oAuthAccessTokenSecret = "JvS4IFjcdTuhjcKPHQzq0XpbISYCLrj1EsRGyVAkygANb";

		ConfigurationBuilder config = new ConfigurationBuilder();

		config.setDebugEnabled(true).setOAuthConsumerKey(oAuthConsumerKey)
				.setOAuthConsumerSecret(oAuthConsumerSecret)
				.setOAuthAccessToken(oAuthAccessToken)
				.setOAuthAccessTokenSecret(oAuthAccessTokenSecret);

		TwitterFactory tfactory = new TwitterFactory(config.build());
		twitter = tfactory.getInstance();
		
	}

	public Long addTweet(final TweetDTO tweetDTO){
		
		return tweetDao.put(modelMapper.map(tweetDTO, Tweet.class));
	}

	public TweetResponse getByHashtag(final String key, final String nextPageQuery, final Long maxId) throws TwitterException{
  		
  		List<String> list = new ArrayList<>();
        TweetResponse tr = new TweetResponse();
        
        System.out.println("Key: " + key);
  		
  		Query query = new Query(key);
        System.out.println(nextPageQuery  + " || "+maxId);
        if (maxId != null)
            query.setMaxId(maxId);

  		query.setCount(5);
        System.out.println("Query: "+ query);
        QueryResult result = twitter.search(query);

	    for (Status status : result.getTweets()) {

	        String url = "https://twitter.com/" + status.getUser().getScreenName() + "/status/" + status.getId();

			OEmbedRequest oembedreq = new OEmbedRequest(status.getId(), url);
			oembedreq.setHideMedia(true);
			oembedreq.setMaxWidth(100);

			TweetsResources tweetsresources = twitter.tweets();
			OEmbedJSONImpl embed = (OEmbedJSONImpl) tweetsresources
					.getOEmbed(oembedreq);

			String html = embed.getHtml();
			list.add(embed.getHtml());

            System.out.println("@" + status.getUser().getScreenName() + ":" + status.getText());
            System.out.println("===========================");
        
	    }

		//check next query
		if (result.hasNext()){
            query = result.nextQuery();
		}

        tr.setOembed(list);
        tr.setMaxId(query.getMaxId());
        tr.setNextPageQuery("?" +
                "max_id=" + query.getMaxId() +
                "&q=" + query.getQuery() + "" +
                "&count=" + query.getCount() +
                "&include_entities=1");
        return tr;
  		
  	}
}
